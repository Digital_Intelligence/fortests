require: functions.js

patterns:
    $hello = (салют|привет|здравствуй*|здарова|добрый (день|вечер))
    $what = (~что|што|что именно|чтоо|че|чё|шо)
    $which = (каки*|коки*)
    $how = (как|каким образом)
    $Yes = (да|конечно|давай|давайте|ага|ок)
    $No = (нет|не|да ну|не хочу|нехочу|да не)
    $Number = $regexp<\b[0-9]+\b>
    $GoodBye = (пока|до встречи|до завтра|до скорого|покасики|прощай|прощайте|до свидания|досвидания|до свиданья|досвиданья|прощайте)
    
theme: /Rules
    state: Rules || noContext = true
        q!: * $what [* это *] за игра *
        q!: * {$which правила} *
        q!: * {$what * делать} *
        q!: * правила * [игры] *
        q!: * ($which|$what) * правила * [игры] *
        q!: * $how * [играть *]
        a: Я задумываю тайное 4-значное число с неповторяющимися цифрами. Ты пытаешь его угадать. Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое мне. Я говорю в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров) и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).     
            
    state: Thanks || noContext = true
        q: * (спасибо|благодарю|спасибки|хорошо|спс)
        a: Пожалуйста.
        go!: /Game   
        
theme: /Game
    state: Starter
        script:
            $session.text = RandomNumberGenerate()
            $reactions.answer('Число загадано, угадывай=)')
        #    $reactions.answer("Загадано - {{$session.text}}");
            
    state: Stopper
        q: (Стоп|надоело|хватит|прекрати|прекратите|сдаюсь)
        q: * Не хочу [больше] *
        a: Ладно, но сдаваться никогда не стоит. Это было число {{$session.text}}
        go!: /Game
            
    state: Guesser
        q: $Number
        script:
            $session.number = $parseTree._Number;
            var checker = FoolCheck($session.number);
            if(checker !== true){
                $reactions.answer(checker);
                $reactions.transition("/Game");
            }
            else{
                if ($session.number == $session.text){
                    $reactions.answer("Мои поздравления! Это загаданное мной число. Сыграем еще?");
                    $reactions.transition("/Game/OnceMore?");
                }
                else{
                    var bool_count = BoolCounter($session.number, $session.text);
                    var cow_count = CowCounter($session.number, $session.text);
        #            $reactions.answer('Быки - ' + bool_count + ' .Коровы - ' + cow_count);
                    var result = CleanPrint(bool_count, cow_count);
                    $reactions.answer(result);
                    $reactions.answer("Попробуй еще");
                    $reactions.transition("/Game");
                }
            }
    state: WrongGuesser
        q: * $Number *
        a: Нужно только четырехзначное число и больше ничего!

    state: OnceMore?
        state:
            q: * $Yes *
            a: Отлично, приступим.
            go!: /Game/Starter
        
        state:
            q: * $No *
            a: Ладно, если захочешь сыграть, обращайся.
            
        state: Yes
            q: * $Yes *
            q: * (хорошо|договорились|обязательно|окейси) *
            a: Ловлю на слове!
            go!:/Game
        
        state: No
            q: * $No *
            a: Ну и ладно. Многое теряешь
            
theme: /Start
    state: Start
        q!: $regex</start>
        a: Привет. Я чат-бот, с которым можно поиграть в игру Быки и Коровы.
    
        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
        
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
        
    state: Ask_to_play
        q!: * ~игра *
        q!: * (~играть|~сыграть) *
        q!: * {хочу * (сыграть|играть|поиграть)} *
        q!: * {(давай|давайте) * (играть|поиграем)} *
        q!: * (давай|давайте) *
        q!: {~быки и ~коровы}
        a: Давай!
        go!: /Game/Starter
    
    state: Hello
        q: * $hello *
        a: Хочешь сыграть в Быков и Коров?    

        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет|хочу|буду) *
            go!: /Game/Starter
            
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
            
theme: /SomeCommonThemes
    state: Hello
        q!: * $hello *
        a: Привет! Хочешь сыграть в Быков и Коров?
        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
            
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
            
    state: HDYD
        q!: * {как * (дела|делишки|делишьки|поживаешь|поживаеш|поживаете)} *
        a: В целом хорошо, но будет еще лучше, если ты сыграешь со мной в Быков и Коров!
        
        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
        
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
            
    state: WDYD
        q!: * {$what * (делаешь|занимаешься|маешься|занимаешся|маешся|делаете|занимаетесь)} *
        a: Готовлюсь поиграть в Быков и Коров. Хочешь?
        
        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
        
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
            
    state: WMCYD
        q!: * {$what * [еще] * (умеешь|умеете) * [делать]} *
        a: Собственно, ничего. Только играть умею. Партейку?
        
        state:
            q: * во что *
            a: Быки и Коровы. Хочешь сыграть?
            
            state: No
                q: * $No *
                q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
                a: Ладно, но, если захочешь, просто спроси.
        
            state: Yes
                q: * $Yes *
                q: * (давай|сыграем|а почему бы и нет) *
                go!: /Game/Starter
            
            state: How
                q: * (как|каким образом) * [играть *]
                q: * $what *
                q: * $how *
                go!: /Rules/Ruless
            
        state: No
            q: * $No *
            q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
            a: Ладно, но, если захочешь, просто спроси.
        
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
        
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
            
theme: /Unknown
    state: CatchAll || noContext = true
        q!: *
        a: Прости, я не знаю про это. Я умею только играться в Быков и Коров.
    
        state: No
                q: * $No *
                q: * (не хочу|не буду|не в настроении|потом|не сейчас|несейчас|нехочу|небуду) *
                a: Ладно, но, если захочешь, просто спроси.
            
        state: Yes
            q: * $Yes *
            q: * (давай|сыграем|а почему бы и нет) *
            go!: /Game/Starter
        
        state: How
            q: * (как|каким образом) * [играть *]
            q: * $what *
            q: * $how *
            go!: /Rules/Rules
        
    state: GoodBye
        q!: * $GoodBye
        a: Пока. Если захочешь поиграть, заходи ко мне
        
        state: Yes
            q: * $Yes *
            q: * (хорошо|договорились|обязательно|окейси) *
            a: Ловлю на слове!
            go!:/Game
        
        state: No
            q: * $No *
            a: Ну и ладно. Многое теряешь
            