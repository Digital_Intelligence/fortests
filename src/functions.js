function random4DigitNumberNotStartingWithZero(){
    var digits = "123456789".split(''),
        first = shuffle(digits).pop();
    digits.push('0');
    return parseInt( first + shuffle(digits).join('').substring(0,3), 10);
}

function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function RandomNumberGenerate(){
    return String(random4DigitNumberNotStartingWithZero());
}

function BoolCounter(gues_num, sec_num){
    var counter = 0;
    for(var i = 0; i < 4; i++){
        if(gues_num[i] == sec_num[i])
            counter++;
    }
    return counter;
}

function CowCounter(gues_num, sec_num){
    var counter = 0;
    for(var i = 0; i < 4; i++){
        for(var j = 0; j < 4; j++){
            if((gues_num[i] == sec_num[j]) && (i != j))
                counter++
        }
    }
    return counter;
}

function CleanPrint(bool_count, cow_count){
    var number_to_word = {
        1:['один', 'одна'],
        2:['два', 'две'],
        3:['три','три'],
        4:['четыре', 'четыре']
    };
    
    var result = '';
    if(bool_count == 0 && cow_count !== 0){
        if(cow_count == 1)
            result += number_to_word[cow_count][1] + ' "корова"';
        else 
            result += number_to_word[cow_count][1] + ' "коровы"';
    }
    else if (cow_count == 0 && bool_count !== 0){
        if(bool_count == 1)
            result += number_to_word[bool_count][0] + ' "бык"';
        else 
            result += number_to_word[bool_count][0] + ' "быка"';
    }
    else if (bool_count == 0 && cow_count == 0){
        result = 'Всего по нулям'
    }
    else{
        if(bool_count == 1)
            result += number_to_word[bool_count][0] + ' "бык", ';
        else 
            result += number_to_word[bool_count][0] + ' "быка", ';
        if(cow_count == 1)
            result += number_to_word[cow_count][1] + ' "корова"';
        else 
            result += number_to_word[cow_count][1] + ' "коровы"';
    }

    return result
}

function FoolCheck(num){
    if(num[0] == 0)
        return 'Число не может же начинаться на 0! Давай по новой'
    if(num.length > 4)
        return 'Слишком длинное число! Введи четырехначное'
    else if (num.length < 4)
        return 'Слишком короткое число! Введи четырехначное'
   var map = {};
   for (var i = 0; i < 4; i++) {
       if (map[num[i]]) return 'В числе не должно быть повторых цифр! Давай еще раз, но без повторов';
        map[num[i]] = 1;
    }
    return true
}